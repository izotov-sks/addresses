<?php

namespace Framework;

interface RepositoryInterface
{
    public function find($id);
    public function save($object);
    public function remove($object);
}