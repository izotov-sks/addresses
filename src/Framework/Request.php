<?php

namespace Framework;

class Request
{
    private $requestUrl;
    private $requestMethod;
    private $parameters;

    public function __construct()
    {
        $requestMethod = (
            isset($_POST['_method'])
            && ($_method = strtoupper($_POST['_method']))
            && in_array($_method, array('PUT', 'DELETE'))
        ) ? $_method : $_SERVER['REQUEST_METHOD'];

        $requestUrl = $_SERVER['REQUEST_URI'];
        if (($pos = strpos($requestUrl, '?')) !== false) {
            $requestUrl = substr($requestUrl, 0, $pos);
        }

        $this->requestMethod = $requestMethod;
        $this->requestUrl = $requestUrl;
    }

    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    public function getRequestUrl()
    {
        return $this->requestUrl;
    }

    public function setParameters($params)
    {
        $this->parameters = $params;
    }

    public function getParameters($param = null)
    {
        if (isset($this->parameters['_params'][$param])) {
            return $this->parameters['_params'][$param];
        } else {
            return $this->parameters;
        }
    }
}
