<?php

namespace Framework;

abstract class BaseController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    abstract public function index();

    public function getRequest()
    {
        return $this->request;
    }

    public function sendResponse($data)
    {
        header('Content-Type: application/json');
        //var_dump($data);
        //echo json_encode($data->jsonSerialize());
        echo json_encode($data);
    }
}
