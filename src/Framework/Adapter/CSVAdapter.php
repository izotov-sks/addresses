<?php

namespace Framework\Adapter;

use Address\Model\Address;

class CSVAdapter
{
    private $rows;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getConfigParameter($key)
    {
        return $this->config[$key];
    }

    public function delete($id)
    {
        $this->rows = $this->getRows();
        if (isset($this->rows[$id])) {
            unset($this->rows[$id]);
        }
        $this->persist();

        return true;
    }

    public function create($object)
    {
        return $object;
    }

    public function update(array $row, $id)
    {
        $this->rows = $this->getRows();
        $this->setRow($row, $id);
        $this->persist();

        return $row;
    }

    public function setRow($data, $id = null)
    {
        if ($id) {
            $this->rows[$id] = $data;
        } else {
            $this->rows[] = $data;
        }
    }

    public function find($rowNum = null)
    {
        $addressCollection = array();
        //$addressCollection = Address::registerCollection();
        if (($handle = fopen($this->getConfigParameter('file'), 'r')) !== false) {
            $id = 0;
            while (($data = fgetcsv($handle)) !== false) {
                $address = new Address();
                $address->id = $id;
                $address->name = (isset($data[0]) ? $data[0] : '');
                $address->phone = (isset($data[1]) ? $data[1] : '');
                $address->street = (isset($data[2]) ? $data[2] : '');
                array_push($addressCollection, $address);
                $id++;
            }
            fclose($handle);
        }

        //var_dump($addressCollection[$rowNum]);die();

        return $rowNum ? $addressCollection[$rowNum] : $addressCollection;
    }

    public function persist()
    {
        if (($handle = fopen($this->getConfigParameter('file'), 'w')) !== false) {
            foreach ($this->rows as $fields) {
                fputcsv($handle, $fields);
            }
        }
    }
}
