<?php

namespace Framework\Adapter;

class DatabaseAdapter
{
    private $connection;
    private $config;
    private static $instance;

    public static function readConfig()
    {
        $config = require PROJECT_ROOT_PATH.'/app/config/config.php';
        return $config;
    }

    public function __construct()
    {
        $this->config = self::readConfig();
    }

    public function getConnection()
    {
        if (is_null($this->connection)) {
            $adapterClass = '\Framework\Adapter\\'.$this->config['adapter'].'Adapter';
            if(class_exists($adapterClass))
                $this->connection = new $adapterClass($this->config);
            else new \Exception('No '.$adapterClass.' was found');
        }

        return $this->connection;
    }

    public static function getInstance()
    {
        if(is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }



}