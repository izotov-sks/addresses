<?php

namespace Framework;

class Dispatcher
{
    private $router;

    public function __construct(RESTRouter $router)
    {
        $this->router = $router;
    }

    public function handle(Request $request)
    {
        $params = $this->router->match($request);

        if (!$params) {
            throw new \Exception(
                'No routes matched!'
            );
        }

        //TODO: need to refactor
        $request->setParameters($params);
        //var_dump($params);die();
        $params = $request->getParameters();

        $action = strtolower($params['_action']);
        $controller = '\Address\Controller\\'.ucfirst(strtolower($params['_controller'])).'Controller';
        $instance = new $controller($request);
        call_user_func_array(array($instance, $action), array($params));
    }
}
