<?php

namespace Framework;

class StorageAdapter
{
    private $connection;
    private $config;
    protected $data;

    public function __construct()
    {
        $this->config = self::readConfig();
    }

    public static function readConfig()
    {
        $config = require PROJECT_ROOT_PATH.'/app/config/config.php';

        return $config;
    }

    public function getConnection()
    {
        if (!$this->connection) {
            $adapterClass = '\Framework\Adapter\\'.$this->config['adapter'].'Adapter';
            $this->connection = new $adapterClass($this->config);
        }

        return $this->connection;
    }

    public function getRows()
    {
        return $this->getConnection()->getRows();
    }

    public function getRow($rowNum)
    {
        return $this->getConnection()->getRows($rowNum);
    }

    public function insert($row)
    {
        return $this->getConnection()->insert($row);
    }

    public function update($row, $id)
    {
        return $this->getConnection()->update($row, $id);
    }

    public function delete($id)
    {
        return $this->getConnection()->delete($id);
    }
}
