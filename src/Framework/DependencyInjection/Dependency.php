<?php

namespace Framework\DependencyInjection;

class Dependency
{
    private $object;
    private $loader;

    public function __construct($loader)
    {
        $this->loader = $loader;
    }

    public function get()
    {
        if ($this->object === null) {
            $this->object = call_user_func($this->loader);
        }

        return $this->object;
    }
}
