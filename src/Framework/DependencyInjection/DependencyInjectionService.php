<?php

namespace Framework\DependencyInjection;

class DependencyInjectionService
{
    private $container;

    public function __construct()
    {
        $this->container = DependencyContainer::getInstance();
    }

    public function register($identifier, $loader)
    {
        $this->container->add($identifier, $loader);
    }

    public function get($identifier)
    {
        return $this->container->get($identifier);
    }
}
