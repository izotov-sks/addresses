<?php

namespace Framework;

class RESTRouter
{
    private $url;
    private $methods = ['GET', 'POST', 'PUT', 'DELETE'];
    private $routes = [];

    public function __construct()
    {
        $this->readConfig();
    }

    protected function readConfig()
    {
        $this->routes = require PROJECT_ROOT_PATH.'/app/config/router.php';
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $url = (string) $url;
        if (substr($url, -1) !== '/') {
            $url .= '/';
        }
        $this->url = $url;
    }

    public function getMethods()
    {
        return $this->methods;
    }

    public function setMethods(array $methods)
    {
        $this->methods = $methods;
    }

    public function match(Request $request)
    {
        $this->setUrl($request->getRequestUrl());

        foreach ($this->routes as $route) {
            $routeParams = explode('/:', $route['uri']);
            $allowRouteFlag = (
                (in_array($request->getRequestMethod(), (array) $this->getMethods()))
                && (in_array($request->getRequestMethod(), (array) $route['methods']))
                && count($routeParams) == (substr_count($this->getUrl(), '/') - 1)
            );

            $params = $uriParams = array();
            $regExp = '~^/(?P<baseuri>.*?)';

            if ($allowRouteFlag) {
                unset($routeParams[0]);
                foreach ($routeParams as $param) {
                    $regExp .= '/(?P<'.$param.'>.*?)';
                }
                $finalRegExp = $regExp.'/?$~';
                if (preg_match($finalRegExp, $request->getRequestUrl(), $output)) {
                    foreach ($output as $key => $param) {
                        if (strpos($param, '/') !== false) {
                            unset($output[$key]);
                        }
                    }

                    $output = array_filter($output, function ($k) {
                        return !is_integer($k);
                    }, ARRAY_FILTER_USE_KEY);
                    $params['_controller'] = $route['controller'];
                    $params['_params'] = $output;
                    $params['_action'] = $route['action'];
                    $params['_method'] = $request->getRequestMethod();

                    return $params;
                }
            }
        }

        return false;
    }
}
