<?php

namespace Address\Model;

use Framework\RepositoryInterface;

interface AddressRepositoryInterface extends RepositoryInterface
{
    public function find($id = null);
    public function save($address);
    public function remove($address);
}

