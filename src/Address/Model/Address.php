<?php

namespace Address\Model;

class Address
{
    private static $instance;

    //TODO: need to use collection/repository instead... and private visibility
    public $id;
    public $name;
    public $phone;
    public $street;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

    public function createFromRequest($data)
    {
        if($this->isValid($data))
        {
            $this->name = '';
            $this->phone = '';
            $this->street = '';
        }

        //TODO: needs to add message if is not valid

        return $this;

    }

    protected function isValid()
    {
        //TODO: needs to add regexps....
        return true;
    }



}
