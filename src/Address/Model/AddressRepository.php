<?php

namespace Address\Model;

use Framework\Adapter\DatabaseAdapter;

class AddressRepository implements AddressRepositoryInterface
{
    private static $instance;
    protected $db;

    public function __construct(DatabaseAdapter $db)
    {
        $this->db = $db;
    }

    public function find($id = null)
    {
        return $this->db->getConnection()->find($id, 'addresses', 'Address');
    }

    public function save($address)
    {
        $this->db->getConnection($address, 'addresses');
    }

    public function remove($address)
    {
        $this->db->remove($address, 'addresses');
    }

    public static function getInstance()
    {
        if(is_null(self::$instance))
            self::$instance = new self(DatabaseAdapter::getInstance());
        return self::$instance;
    }
}

