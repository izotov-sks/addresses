<?php

namespace Address\Controller;

use Address\Model\AddressRepository;
use Framework\BaseController;
use Address\Model\Address;

class AddressController extends BaseController
{
    public function index()
    {
        $addresses = AddressRepository::getInstance()
            ->find();
        $this->sendResponse($addresses);
    }

    public function show()
    {
        $address = AddressRepository::getInstance()
            ->find($this->getRequest()->getParameters('id'));
        $this->sendResponse($address);
    }

    public function update()
    {
        $params = $this->getRequestParameters();
        $updatedAddress = AddressRepository::getInstance()
            ->update($params, $this->getRequestParameters('id'));
        $this->sendResponse($updatedAddress);
    }

    public function create()
    {
        $params = $this->getRequest()->getParameters();
        $address = new Address();
        $address->createFromRequest($params);
        var_dump($address);die();
        AddressRepository::getInstance()->save($address);
        $this->sendResponse($address);
    }

    public function delete()
    {
        $deletedAddress = Address::getInstance()
            ->delete($this->getRequestParameters('id'));
        $this->sendResponse($deletedAddress);
    }
}
