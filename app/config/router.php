<?php

return [
    [
        'name'          => 'address_list',
        'uri'    => 'address',
        'controller' => 'Address',
        'action'    => 'index',
        'methods'   => ['GET']
    ],
    [
        'name'          => 'address_show',
        'uri'    => 'address/:id',
        'controller' => 'Address',
        'action'    => 'show',
        'methods'   => ['GET']
    ],
    [
        'name'          => 'address_create',
        'uri'    => 'address',
        'controller' => 'Address',
        'action'    => 'create',
        'methods'   => ['POST']
    ],
    [
        'name'          => 'address_update',
        'uri'    => 'address/:id',
        'controller' => 'Address',
        'action'    => 'update',
        'methods'   => ['PUT']
    ],
    [
        'name'          => 'address_delete',
        'uri'    => 'address/:id',
        'controller' => 'Address',
        'action'    => 'delete',
        'methods'   => ['DELETE']
    ]
];