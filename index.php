<?php

use Framework\RESTRouter;
use Framework\Dispatcher;
use Framework\Request;

define('PROJECT_ROOT_PATH', __DIR__);

$loader = require __DIR__ . '/vendor/autoload.php';
$request = new Request();
$router = new RESTRouter();
$dispatcher = new Dispatcher($router);
$dispatcher->handle($request);
